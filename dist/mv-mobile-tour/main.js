(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "agm-map {\n    height: 700px;\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\n  <span>My Application</span>\n  <span class=\"flex\"></span>\n  <mat-menu #appMenu=\"matMenu\">\n    <button mat-menu-item>Settings</button>\n    <button mat-menu-item>Help</button>\n  </mat-menu>\n\n  <button mat-icon-button [matMenuTriggerFor]=\"appMenu\">\n    <mat-icon>more_vert</mat-icon>\n  </button>\n</mat-toolbar>\n\n<agm-map #gmap [latitude]=\"lat\" [longitude]=\"lng\" [mapDraggable]=\"draggable\" [zoom]=\"zoom\"\n         (mapRightClick)=\"handleRightClick($event)\"\n         (mapClick)=\"onMapClick($event)\">\n  <agm-marker\n      *ngFor=\"let m of getMarkers(); let i = index\"\n      [latitude]=\"m.lat\"\n      [longitude]=\"m.lng\"\n      [iconUrl]=\"m.icon\"\n      [markerDraggable]=\"m.draggable\"\n      (markerClick)=\"showInfoWindow(m,i)\"\n      (markerRightClick)=\"handleRightClick(m,i,$event)\"\n      (dragEnd)=\"updateMarkerProps(m,i,$event)\">\n    <agm-circle\n        [latitude]=\"m.lat\"\n        [longitude]=\"m.lng\"\n        [radius]=\"m.circle.radius\"\n        [fillColor]=\"m.circle.fillColor\"\n        [fillOpacity]=\"m.circle.opacity\"\n    ></agm-circle>\n  </agm-marker>\n\n  <agm-polyline\n      [strokeColor]=\"getRoute().color\"\n      [strokeOpacity]=\"getRoute().opacity\"\n      [strokeWeight]=\"getRoute().thickness\">\n    <agm-polyline-point\n        *ngFor=\"let routePoint of getRoute().routePoints; let j = index\"\n        [latitude]=\"routePoint.lat\"\n        [longitude]=\"routePoint.lng\"\n    ></agm-polyline-point>\n  </agm-polyline>\n\n</agm-map>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_MarkerCircle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shared/MarkerCircle */ "./src/app/shared/MarkerCircle.ts");
/* harmony import */ var _shared_StandardMarker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/StandardMarker */ "./src/app/shared/StandardMarker.ts");
/* harmony import */ var _shared_Route__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/Route */ "./src/app/shared/Route.ts");
/* harmony import */ var _shared_RoutePoint__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/RoutePoint */ "./src/app/shared/RoutePoint.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Test Google Maps';
        this.lat = 30.26715;
        this.lng = -97.74306;
        this.zoom = 18;
        this.draggable = true;
        this._markers = [];
        this._routePoints = [];
    }
    AppComponent.prototype.handleRightClick = function (event) {
    };
    AppComponent.prototype.onMapClick = function (event) {
        console.log(event.coords.lat + ' ' + event.coords.lng);
        this._route.routePoints.push(new _shared_RoutePoint__WEBPACK_IMPORTED_MODULE_4__["RoutePoint"](event.coords.lat, event.coords.lng));
    };
    AppComponent.prototype.showInfoWindow = function (m, i) {
    };
    AppComponent.prototype.showRightClickWindow = function (m) {
        alert(m.description);
    };
    AppComponent.prototype.getMarkers = function () {
        return this._markers;
    };
    AppComponent.prototype.getRoute = function () {
        return this._route;
    };
    AppComponent.prototype.updateMarkerProps = function (marker, index, event) {
        marker.lat = event.coords.lat;
        marker.lng = event.coords.lng;
        marker.circle.lat = event.coords.lat;
        marker.circle.lng = event.coords.lng;
    };
    AppComponent.prototype.ngOnInit = function () {
        var circle = new _shared_MarkerCircle__WEBPACK_IMPORTED_MODULE_1__["MarkerCircle"](20);
        var marker = new _shared_StandardMarker__WEBPACK_IMPORTED_MODULE_2__["StandardMarker"]();
        var route = new _shared_Route__WEBPACK_IMPORTED_MODULE_3__["Route"]();
        route.name = 'Test Route';
        var routePoints = [];
        routePoints.push();
        marker.lat = this.lat;
        marker.lng = this.lng;
        marker.title = 'Title';
        marker.description = 'Description';
        marker.circle = circle;
        console.log(marker);
        this._markers.push(marker);
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")],
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _ngmaterial_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ngmaterial.module */ "./src/app/ngmaterial.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAohhYWBxvGDJldAPfaP6ww7tSV39Zgiis'
                }),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _ngmaterial_module__WEBPACK_IMPORTED_MODULE_7__["MaterialAppModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/ngmaterial.module.ts":
/*!**************************************!*\
  !*** ./src/app/ngmaterial.module.ts ***!
  \**************************************/
/*! exports provided: MaterialAppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialAppModule", function() { return MaterialAppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MaterialAppModule = /** @class */ (function () {
    function MaterialAppModule() {
    }
    MaterialAppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"]],
            exports: [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"]]
        })
    ], MaterialAppModule);
    return MaterialAppModule;
}());



/***/ }),

/***/ "./src/app/shared/MarkerCircle.ts":
/*!****************************************!*\
  !*** ./src/app/shared/MarkerCircle.ts ***!
  \****************************************/
/*! exports provided: MarkerCircle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MarkerCircle", function() { return MarkerCircle; });
var MarkerCircle = /** @class */ (function () {
    function MarkerCircle(radius) {
        this.fillColor = "Aquamarine";
        this.opacity = .45;
        this.radius = radius;
    }
    return MarkerCircle;
}());



/***/ }),

/***/ "./src/app/shared/Route.ts":
/*!*********************************!*\
  !*** ./src/app/shared/Route.ts ***!
  \*********************************/
/*! exports provided: Route */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Route", function() { return Route; });
var Route = /** @class */ (function () {
    function Route() {
        this.name = "";
        this.color = "LightBlue";
        this.opacity = 1.0;
        this.thickness = 10;
        this.routePoints = [];
    }
    return Route;
}());



/***/ }),

/***/ "./src/app/shared/RoutePoint.ts":
/*!**************************************!*\
  !*** ./src/app/shared/RoutePoint.ts ***!
  \**************************************/
/*! exports provided: RoutePoint */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutePoint", function() { return RoutePoint; });
var RoutePoint = /** @class */ (function () {
    function RoutePoint(lat, lng) {
        this.lat = lat;
        this.lng = lng;
    }
    return RoutePoint;
}());



/***/ }),

/***/ "./src/app/shared/StandardMarker.ts":
/*!******************************************!*\
  !*** ./src/app/shared/StandardMarker.ts ***!
  \******************************************/
/*! exports provided: StandardMarker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StandardMarker", function() { return StandardMarker; });
var StandardMarker = /** @class */ (function () {
    function StandardMarker() {
        this.autoPlay = true;
        this.autoZoom = false;
        this.draggable = true;
        this.icon = {
            url: '../../assets/GMapMarker.png',
            scaledSize: {
                height: 40,
                width: 40
            }
        };
    }
    StandardMarker.prototype.getId = function () {
        return this._id;
    };
    return StandardMarker;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/jmcguf172/Desktop/mv-mobile-tour/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map