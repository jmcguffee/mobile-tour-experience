import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Marker} from "../shared/Marker";


@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  constructor(private http: HttpClient) { }

  public getMarkers() : Observable<Array<Marker>>{
    return this.http.get<Array<Marker>>("http://localhost:8080/api/marker");
  }
}
