import {Component, Directive, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import { Marker } from '../shared/Marker';
import { Route } from '../shared/Route';
import { GoogleMapsAPIWrapper} from '@agm/core';
import { } from '@types/googlemaps';
import {StandardMarker} from "../shared/StandardMarker";
import {MatDrawer} from "@angular/material";
import {MarkerService} from "../services/marker.service";


@Directive({
  selector: 'native-map'
})
export class NativeMap{
  public map;
  constructor(private _wrapper : GoogleMapsAPIWrapper){
    this._wrapper.getNativeMap().then(map => {
      this.map = map;
    })

  }

}

@Component({
  selector: 'app-map-editor',
  templateUrl: './html/map-editor.component.html',
  styleUrls: ['./css/map-editor.component.css'],
})
export class MapEditorComponent implements OnInit {

  constructor(private _markerService : MarkerService){
    _markerService.getMarkers().subscribe(markers => {
      this._markers = markers
      console.log(this._markers);
    });
  }


  @HostListener('document:keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    if (event.keyCode === 27) {
      if(this._drawer && this._drawer.opened){
        this._drawer.toggle();
      }
    }
  }


  lat: number = 30.26715;
  lng: number = -97.74306;
  zoom: number = 18;
  draggable: boolean = true;


  styles = [
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.attraction",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.government",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.medical",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.place_of_worship",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.school",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ];

  @ViewChild(NativeMap) _nativeMap: NativeMap;

  private _drawer : MatDrawer;
  private _markers : Array<Marker> = [];
  private _route: Route = new Route();
  private _activeMarker: Marker = new StandardMarker();

  public getMarkers() : Array<Marker> {
    return this._markers;
  }

  public addMarker(event){
    let latLng = this.point2LatLng({ x: event.layerX, y: event.layerY}, this._nativeMap.map);
    let data = event.dataTransfer.getData("URL");
    let marker = new StandardMarker();
    marker.lat = latLng.lat();
    marker.lng = latLng.lng();
    marker.icon.url = data;
    marker.title = "Marker - " + (this._markers.length + 1);
    this._markers.push(marker);
  }

  public showMarkerWindow(drawer: MatDrawer, marker: Marker, index: number){
    if(this._drawer === null || typeof this._drawer === 'undefined'){
      this._drawer = drawer;
    }
    if(!this._drawer.opened){
      drawer.toggle();
    }
    this._activeMarker = marker;
  }

  public updateMarkerProps(marker, index, event){
    marker.lat = event.coords.lat;
    marker.lng = event.coords.lng;
    marker.circle.lat = event.coords.lat;
    marker.circle.lng = event.coords.lng;
  }

  public onDragOver(event){
    event.preventDefault();
    event.stopPropagation();
  }

  private point2LatLng(point, map) {
    let topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    let bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    let scale = Math.pow(2, map.getZoom());
    let worldPoint = new google.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
    return map.getProjection().fromPointToLatLng(worldPoint);
  }

  public getActiveMarker(){
    return this._activeMarker;
  }

  ngOnInit() {

  }

}
