import { NgModule } from "@angular/core";
import {
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule, MatSliderModule
} from "@angular/material";

@NgModule({
    imports: [
      MatButtonModule,
      MatToolbarModule,
      MatMenuModule,
      MatIconModule,
      MatToolbarModule,
      MatFormFieldModule,
      MatInputModule,
      MatSliderModule
    ],
    exports: [
      MatButtonModule,
      MatToolbarModule,
      MatMenuModule,
      MatIconModule,
      MatToolbarModule,
      MatFormFieldModule,
      MatInputModule,
      MatSliderModule
    ]
})
export class MaterialAppModule{

}