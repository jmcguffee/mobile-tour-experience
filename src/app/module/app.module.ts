import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../components/app.component';
import { AgmCoreModule } from '@agm/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialAppModule } from './ngmaterial.module';
import { RouterModule, Routes } from '@angular/router'
import { MapEditorComponent, NativeMap } from "../components/map-editor.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import {MatCard, MatCardTitle, MatDrawer, MatDrawerContainer, MatDrawerContent, MatSlider} from "@angular/material";
import {ColorPickerModule} from "ngx-color-picker";


const appRoutes : Routes = [
  { path : 'map-editor', component: MapEditorComponent, data: { title: 'Map Editor' }}
];

@NgModule({
  declarations: [
    AppComponent,
    MapEditorComponent,
    NativeMap,
    MatDrawerContainer,
    MatDrawer,
    MatDrawerContent,
    MatCard,
    MatCardTitle
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey : 'AIzaSyAohhYWBxvGDJldAPfaP6ww7tSV39Zgiis'
    }),
    BrowserAnimationsModule,
    MaterialAppModule,
    RouterModule.forRoot(appRoutes),
    NgProgressModule,
    HttpClientModule,
    ColorPickerModule

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule{

}
