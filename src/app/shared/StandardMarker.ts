import {Marker} from "./Marker";
import {MarkerCircle} from "./MarkerCircle";

export class StandardMarker implements Marker{

    constructor(){}

    _id : number;
    public audio: string;
    public autoPlay: boolean = true;
    public autoZoom: boolean = false;
    public circle: MarkerCircle = new MarkerCircle(20);
    public description: string;
    public draggable: boolean = true;
    public icon = {
        url: '../../assets/GMapMarker.png',
        scaledSize: {
            height: 40,
            width: 40
        }
    };
    public lat: number;
    public lng: number;
    public title: string;
    public animation: string = 'BOUNCE';

    public getId(){
        return this._id;
    }
}