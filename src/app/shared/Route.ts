import { RoutePoint } from './RoutePoint';

export class Route {
  public origin: RoutePoint;
  public destination: RoutePoint;
}