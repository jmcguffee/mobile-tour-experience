export class Bounds{

  public constructor(lat: number, lng: number){
    this.lat = lat;
    this.lng = lng;
  }

  public lat;
  public lng;
}