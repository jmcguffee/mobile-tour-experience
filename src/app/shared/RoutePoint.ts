export class RoutePoint{

  public constructor(lat: number, lng: number){
    this.lat = lat;
    this.lng = lng;
  }

  public id: number;
  public lat: number;
  public lng: number;
}