import {MarkerCircle} from "./MarkerCircle";

export interface Marker{
    _id: number;
    lat : number;
    lng : number;
    draggable : boolean;

    title: string;
    description : string;

    //Custom marker image
    icon?: any;

    audio?: string;

    //Defines the distance from the marker before playing audio
    circle? : MarkerCircle;

    //Starts playing audio when locator hits bubble
    autoPlay?: boolean;

    //Will Zoom to marker when locator hits bubble;
    autoZoom?: boolean;

    animation?: string;

}