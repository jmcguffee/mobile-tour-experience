export class MarkerCircle {

    public constructor(radius : number){
        this.radius = radius;
    }

    public radius : number;
    public fillColor: string = "Aquamarine";
    public opacity: number = .45;
}